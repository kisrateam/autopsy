<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAutopsy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_autopsy',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('hospcode');
            $table->integer('usertype_id');
            $table->string('hospname');
            $table->string('email');
            $table->string('phone_number');
            $table->string("password");
            $table->string("vip_password");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_autopsy');
    }
}
