import Service from "../service";

const Auth = {
    login(data) {
        return Service.post("/login", data);
    },
    register(data){
        return Service.post('/register',data);
    },
    update(id, data) {
        return Service.post("/update/" + id, data);
    },
    getUserAll() {
        return Service.get("/get_user");
    },
    logout(data) {
        return Service.post("/logout",data);
    },
};

export default Auth;
