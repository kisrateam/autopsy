import Service from "../service";
// import AuthService from "../AuthService";

const Common = {
    getProvince(user_id) {
        return Service.get("/provinces?user_id="+user_id);
    },
    getDistrict(province_code){
        return Service.get('/districts?province_code='+province_code);
    },
    getSubDistrict(district_code){
        return Service.get('/sub_districts?district_code='+district_code);
    },
    getTypeWater(){
        return Service.get('/type_water');
    },
    getDepartments(){
        return Service.get('/departments');
    },
    getProvincePatient() {
      return Service.get('/province_patient');
    },
    getNation() {
      return Service.get('/nation');
    },
    getHospital(province_code) {
      return Service.get('/hospital?province_code='+province_code);
    }
};


export default Common;
