import Service from "../service";
import AuthService from "../AuthService";

const Patient = {
    getPatient(page,field,sortype,area_id,user_type_id,province_id) {
        return Service.get("/patient?page="+page+
          "&field="+field+
          "&sorttype="+sortype+
          "&area_id="+area_id+
          "&user_type_id="+user_type_id+
          "&province_id="+province_id);
    },
    showPatient(event_id,order) {
        return Service.get("/patient_show?event_id="+event_id+"&order="+order);
    },
    updatePatient(data) {
        return Service.post("/patient_update",data);
    },
    createPatient(data) {
        return Service.post("/patient", data);
    },
    deletePatient(id) {
        return Service.delete("/patient/" + id);
    },
    searchPatient(data){
        return Service.post('/search_patient',data);
    }
};

export default Patient;
