import Service from "../service";
import AuthService from "../AuthService";

const Event = {
    getEvent(page,field,sortype,area_id,user_type_id,province_id,hospital_code) {
        return Service.get("/event?page="+page+
          "&field="+field+
          "&sorttype="+sortype+
          "&area_id="+area_id+
          "&user_type_id="+user_type_id+
          "&province_id="+province_id+
          "&hospital_code="+hospital_code);
    },
    showEventById(id) {
        return Service.get("/event_show/" + id);
    },
    updateEvent(id, data) {
        return Service.post("/event_update/" + id, data);
    },
    createEvent(data) {
        return Service.post("/event", data);
    },
    deleteEvent(id) {
        return Service.delete("/event/" + id);
    },
    countPatient(){
        return Service.get('/count_patient');
    },
    searchEvent(data){
        return Service.post('/search_event',data);
    },
    getEventAll(){
        return Service.get('/get_event_all');
    },
};

export default Event;
