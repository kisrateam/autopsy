import Auth from "./api/AuthAPI";
import Common from "./api/CommonAPI";
import Event from "./api/EventAPI"
import Patient from "./api/PatientAPI"

export default {
   Auth,
   Common,
   Event,
   Patient,
}
