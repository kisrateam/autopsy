import Service from "../../service/index";

const Common = {
    namespaced: true,
    state: {
        provinces: [],
        districts: [],
        sub_districts: [],
        type_water: [],
        departments: [],
        province_patient:[],
        nations_code:[],
        hospitals:[],
    },
    mutations: {
        GET_PROVINCES: (state, data) => {
            state.provinces = data;
        },
      GET_PROVINCES_PATIENT: (state, data) => {
        state.province_patient = data;
      },
        GET_DISTRICTS: (state, data) => {
            state.districts = data;
        },
        GET_SUB_DISTRICTS: (state, data) => {
            state.sub_districts = data;
        },
        GET_TYPE_WATER: (state, data) => {
            state.type_water = data;
        },
        GET_DEPARTMENTS: (state, data) => {
            state.departments = data;
        },
      GET_NATION: (state, data) => {
        state.nations_code = data;
      },
      GET_HOSPITAL: (state, data) => {
        state.hospitals = data;
      },

    },
    actions: {
        setProvince({commit},user_id){
            return Service.Common.getProvince(user_id)
                .then(data=>{
                    commit("GET_PROVINCES",data);
                })
        },
        setDistrict({commit},province_code){
            return Service.Common.getDistrict(province_code)
                .then(data=>{
                    commit("GET_DISTRICTS",data);
                })
        },
        setSubDistrict({commit},district_code){
            return Service.Common.getSubDistrict(district_code)
                .then(data=>{
                    commit("GET_SUB_DISTRICTS",data);
                })
        },
        setTypeWater({commit}){
            return Service.Common.getTypeWater()
                .then(data=>{
                    commit("GET_TYPE_WATER",data);
                })
        },
        setDepartments({commit}){
            return Service.Common.getDepartments()
                .then(data=>{
                    commit("GET_DEPARTMENTS",data);
                })
        },
      setProvincePatient({commit}){
        return Service.Common.getProvincePatient()
          .then(data=>{
            commit("GET_PROVINCES_PATIENT",data);
          })
      },
      setNation({commit}){
        return Service.Common.getNation()
          .then(data=>{
            commit("GET_NATION",data);
          })
      },
      setHospital({commit},province_code){
        return Service.Common.getHospital(province_code)
          .then(data=>{
            commit("GET_HOSPITAL",data);
          })
      },
    }
};

export default Common;
