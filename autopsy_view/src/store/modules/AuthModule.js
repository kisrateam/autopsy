import Service from "../../service/index";

const Auth = {
    namespaced: true,
    state: {
        test : "",
        user: [],
        user_login: null,
        form: {
            first_name: "",
            last_name : "",
            sid : "",
            phone_number: "",
            email: "",
        },
        login_form:{
            sid: "",
            phone_number:"",

        },
        accessToken: null,
        registerToken : null,
    },
    mutations: {
        SET_USER: (state, user) => {
            state.user = user;
        },
        SET_USER_LOGIN: (state, user) => {
            state.user_login = user;
        },
        SET_TOKEN_REGISTER : (state, token) => {
            state.registerToken = token;
        },
        SET_TOKEN: (state, token) => {
            state.accessToken = token;
        },
        CLEAR_FORM_DATA: state => {
            state.form = {
                first_name: "",
                last_name : "",
                sid : "",
                phone_number: "",
                email: "",
            };
        },
    },
    actions: {
        login({ commit }, data) {
            return Service.Auth.login(data).then(response =>{
                commit("SET_USER_LOGIN", response);
            });
        },
        register({ commit }, data) {
            return Service.Auth.register(data).then(response => {
                commit("SET_TOKEN_REGISTER", response.success.token);
                commit("SET_USER", response);
                return data;
            });
        },
        logout({ commit }) {
            commit("SET_USER_LOGIN", null);
        },
        clearFormData({ commit }) {
            commit("CLEAR_FORM_DATA");
        },
        setUser({commit}) {
            return Service.Auth.getUserAll().then(res => {
                commit("SET_USER", res);
            });
        },
        getToken({ commit }, data) {
            return Service.Auth.getToken(data).then(data => {
                commit("SET_TOKEN", data.success.token);
            }).catch(err => Promise.reject(err));
        },
        getTest({commit}, data){
            commit("TEST",data);
        }
    }
};

export default Auth;
