import Service from "../../service/index";

const Event = {
    namespaced: true,
    state: {
        event: null,
        optionEvent: [],
        patient_all: [],
        form: {
            id:"",
            user_id: "",
            name: "",
            role: "",
            department: "",
            department_id: "",
            province_department: "",
            province_department_id: "",
            phone_number_department: "",
            fax_number_department: "",
            report_date: "",
            location: "",
            lat: "",
            lng: "",
            village_no: "",
            location_province: "",
            location_district: "",
            location_sub_district: "",
            location_province_id: "",
            location_district_id: "",
            location_sub_district_id: "",
            type_event: "",
            date_of_event: "",
            time_of_event: "",
            total_patient: "",
            death_patient: '',
            injury_patient: '',
            no_injury_patient: '',
            cause_of_event: "",
            activity_during_event: "",
            proceed_after_event: "",
            type_water: "",
            type_water_id: "",
            depth_level_metre: "",
            depth_level_centimeter: "",
            has_management_risk_before_id: "",
            management_risk_before_id: [],
            has_management_risk_after_id: "",
            management_risk_after_id: [],
            other_management_risk_before: "",
            other_management_risk_after: "",
            patient : null
        },
        startLocation: {
            lat: 13.7649084 ,
            lng: 100.5382846
        },
        dataPaginate: {
            current_page: 1,
            from: 1,
            last_page: 1,
            per_page: 1,
            total: 1,
            to: 1
        },
        search:{
            name: "",
            province: "",
            start_date: "",
            end_date: "",
        }
    },
      mutations: {
          SET_EVENT:(state,res)=>{
            state.event = res.data;
            state.dataPaginate = {
                current_page: res.current_page,
                from: res.form,
                last_page: res.last_page,
                per_page: res.per_page,
                total: res.total,
                to: res.to
            };
        },
        GET_EVENT_BY_ID:(state,res)=>{
            state.form = res;
            state.startLocation ={
                lat: res.lat,
                lng: res.lng
            }
        },
        GET_PATIENT_ALL:(state,res)=>{
            state.patient_all = res;
        },
        GET_EVENT_ALL:(state,res)=>{
            state.optionEvent = res;
        },
        SET_FORM_EVENT:(state,res)=>{
            state.form = res;
        },
        CLEAR_FORM_DATA: state => {
            state.form = {
                id :"",
                user_id: "",
                name: "",
                role: "",
                department: "",
                department_id: "",
                province_department: "",
                province_department_id: "",
                phone_number_department: "",
                fax_number_department: "",
                report_date: new Date().toISOString().slice(0,10),
                lat: "",
                lng: "",
                location: "",
                village_no: "",
                location_province: "",
                location_district: "",
                location_sub_district: "",
                location_province_id: "",
                location_district_id: "",
                location_sub_district_id: "",
                location_id: "",
                type_event: "",
                date_of_event: "",
                time_of_event: "",
                death_patient: '',
                injury_patient: '',
                no_injury_patient: '',
                cause_of_event: "",
                activity_during_event: "",
                proceed_after_event: "",
                type_water: "",
                type_water_id: "",
                type_water_description: "",
                depth_level_metre: "",
                depth_level_centimeter: "",
                has_management_risk_before_id: "",
                management_risk_before_id: [],
                has_management_risk_after_id: "",
                management_risk_after_id: [],
                other_management_risk_before: "",
                other_management_risk_after: "",
                patient : null
            };
            state.startLocation ={
                lat: 13.7649084 ,
                lng: 100.5382846
            }
        },
        CLEAR_SEARCH: state => {
            state.search = {
                name: "",
                province: "",
                start_date: "",
                end_date: "",
            };
        },
    },
    actions: {
        setEvent({commit},data) {
          return Service.Event.getEvent(data.page, data.field, data.sortType,data.area_id,data.user_type_id,data.province_id,data.hospital_code).then(res => {
                commit("SET_EVENT", res);
            });
        },
        getPatientAll({commit}) {
            return Service.Event.countPatient().then(res => {
                commit("GET_PATIENT_ALL", res);
            });
        },
        setEventAll({commit}) {
            return Service.Event.getEventAll().then(res => {
                commit("GET_EVENT_ALL", res);
            });
        },
        saveEvent({commit},{id,data}){
            if(!id){
                return Service.Event.createEvent(data).then(res=>{
                    commit("SET_FORM_EVENT",res);
                });
            }
            else{
                return Service.Event.updateEvent(id,data).then(res=>{
                    console.log("update");
                });
            }
        },
        showEvent({commit},id){
            return Service.Event.showEventById(id).then(res=>{
                commit("GET_EVENT_BY_ID",res);
            })
        },
        search({commit},data){
            return Service.Event.searchEvent(data).then(res=>{
                commit("SET_EVENT",res)
            })
        },
        clearFormData({ commit }) {
            commit("CLEAR_FORM_DATA");
        },
        clearSearch({ commit }) {
            commit("CLEAR_SEARCH");
        },
    }
};

export default Event;
