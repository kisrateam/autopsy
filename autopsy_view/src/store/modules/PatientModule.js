import Service from "../../service/index";

const Patient = {
    namespaced: true,
    state: {
        patient: null,
        form: {
            id: "",
            event_id: "",
            order: "",
            unknown_person: "",
            alive: "",
            first_name: "",
            last_name: "",
            id_number: "",
            nationality_code:{
              "id": 92,
              "code": "099",
              "nation": "ไทย"
            },
            nationality_code_id:"",
            address_no: "",
            village_no: "",
            location_province: "",
            location_district: "",
            location_sub_district: "",
            location_province_id: "",
            location_district_id: "",
            location_sub_district_id: "",
            school:'',
            gender_id: "",
            age: "",
            age_month: "",
            patient_status_id: "",
            injure_patient_status_id: "",
            other_injure_patient_status: "",
            date_of_illness: "",
            date_of_treat: "",
            date_of_death: "",
            swimming_ability_id: "",
            swimming_id: "",
            people_with_patient_id: "",
            activity_people_with_patient_id: "",
            other_activity_people_with_patient: "",
            activity_before_event_id: [],
            other_activity_before_event: "",
            caregiver_id: [],
            other_caregiver: "",
            distance_kilometre: "",
            distance_metre: "",
            has_event_at_home_id: false,
            has_floating_device_id: "",
            floating_device_id: "",
            other_floating_device:"",
            has_life_jacket_id: "",
            risk_factors_id: [],
            other_risk_factors: "",
            rescuer_id: [],
            type_help_id: [],
            throwing_tool: "",
            hand_tool: "",
            other_type_help: "",
            drowning_time_hr:"",
            drowning_time: "",
            has_first_aid_id: "",
            reason_not_first_aid_id: [],
            type_people_first_aid_id: [],
            other_type_people_first_aid: "",
            type_first_aid_id: [],
            other_type_first_aid_id: "",
            has_type_deliver_id: "",
            type_deliver_id: "",
            other_deliver_type: "",
        },
        dataPaginate: {
            current_page: 1,
            from: 1,
            last_page: 1,
            per_page: 1,
            total: 1,
            to: 1
        },
        onSave : false,
        search: {
            event_id: "",
            name : "",
            province:"",
            gender: "",
        }
    },
    mutations: {
        SET_PATIENT:(state,res)=>{
            state.patient = res.data;
            state.dataPaginate = {
                current_page: res.current_page,
                from: res.form,
                last_page: res.last_page,
                per_page: res.per_page,
                total: res.total,
                to: res.to
            };
        },
        SET_FORM_PATIENT:(state,res)=>{
            state.form = res;
        },
        GET_PATIENT_BY_ID:(state,res)=>{
            state.form = res;
        },
        SET_ORDER : (state,index)=>{
            console.log(index);
            state.form.order = index;
        },
        CLEAR_FORM_DATA: state => {
            state.form = {
                id: "",
                event_id: "",
                unknown_person: "",
                alive: "",
                first_name: "",
                last_name: "",
                id_number: "",
                nationality_code:{
                  "id": 92,
                  "code": "099",
                  "nation": "ไทย"
                },
                nationality_code_id:"",
                address_no: "",
                village_no: "",
                location_province: "",
                location_district: "",
                location_sub_district: "",
                location_province_id: "",
                location_district_id: "",
                location_sub_district_id: "",
                school:"",
                gender_id: "",
                age: "",
                age_month: "",
                patient_status_id: "",
                injure_patient_status_id: "",
                other_injure_patient_status: "",
                date_of_illness: "",
                date_of_treat: "",
                date_of_death: "",
                swimming_ability_id: "",
                swimming_id: "",
                people_with_patient_id: "",
                activity_people_with_patient_id: "",
                other_activity_people_with_patient: "",
                activity_before_event_id: [],
                other_activity_before_event: "",
                caregiver_id: [],
                other_caregiver: "",
                distance_kilometre: "",
                distance_metre: "",
                has_event_at_home_id: false,
                has_floating_device_id: "",
                floating_device_id: "",
                other_floating_device:"",
                has_life_jacket_id: "",
                risk_factors_id: [],
                other_risk_factors: "",
                rescuer_id: [],
                type_help_id: [],
                throwing_tool: "",
                hand_tool: "",
                other_type_help: "",
                drowning_time: "",
                has_first_aid_id: "",
                reason_not_first_aid_id: [],
                type_people_first_aid_id: [],
                other_type_people_first_aid: "",
                type_first_aid_id: [],
                other_type_first_aid_id: "",
                has_type_deliver_id: "",
                type_deliver_id: "",
                other_deliver_type: "",
            }
        },
        CLEAR_SEARCH: state => {
            state.search = {
                event_id: "",
                name : "",
                province:"",
                gender: "",
            }
        }
    },
    actions: {
        setPatient({commit},data) {
            return Service.Patient.getPatient(data.page, data.field, data.sorttype,data.area_id,data.user_type_id,data.province_id).then(res => {
                commit("SET_PATIENT", res);
            });
        },
        savePatient({commit},{id,data}){
            if(!id){
                return Service.Patient.createPatient(data).then(res=>{
                    console.log("create");
                });
            }
            else{
                console.log(data);
                return Service.Patient.updatePatient(data).then(res=>{
                    console.log('update');
                });
            }
        },
        showPatient({commit},{event_id , order}){
            return Service.Patient.showPatient(event_id,order).then(res=>{
                commit("GET_PATIENT_BY_ID",res);
            })
        },
        search({commit},data){
            return Service.Patient.searchPatient(data).then(res=>{
                commit("SET_PATIENT",res)
            })
        },
        clearFormData({ commit }) {
            commit("CLEAR_FORM_DATA");
        },
        setOrder({commit},index){
            commit("SET_ORDER",index);
        },
        clearSearch({ commit }) {
            commit("CLEAR_SEARCH");
        },


    }
};

export default Patient;
