import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Auth from "./modules/AuthModule"
import Common from "./modules/CommonModule"
import Event from "./modules/EventModule"
import Patient from "./modules/PatientModule"

Vue.use(Vuex);

const modules = {
    Auth: Auth,
    Common: Common,
    Event: Event,
    Patient: Patient,
};

export default new Vuex.Store({
    modules,
    plugins: [createPersistedState()]
});
