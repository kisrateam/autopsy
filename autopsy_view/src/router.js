import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/LoginView.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: "/contact_us",
      name: "ContactView",
      component: () =>
          import("./views/ContactView.vue")
    },
    {
      path: '/login',
      name: 'LoginView',
      component: () => import('./views/LoginView.vue')
    },
    {
      path: '/register',
      name: 'RegisterView',
      component: () => import('./views/RegisterView.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/question-form/1',
      name: 'question-form1',
      component: () => import('./views/QuestionForm1.vue')
    },
    {
      path: '/question-form/2',
      name: 'question-form2',
      component: () => import('./views/QuestionForm2.vue')
    },
    {
      path: '/question-form/3',
      name: 'question-form3',
      component: () => import('./views/QuestionForm3.vue')
    },
    // {
    //   path: '/drowning',
    //   name: 'DrowningView',
    //   component: () =>
    //       import('./views/drowningLayouts/DrowningView.vue'),
    //   children:[
    //     {
    //       path: '/event-form',
    //       name: 'EventForm',
    //       props: true,
    //       component: () =>
    //           import('./components/drowning/EventForm.vue')
    //     },
    //     {
    //       path: '/drowning-form',
    //       name: 'DrowningForm',
    //       props: true,
    //       component: () =>
    //           import('./components/drowning/DrowningForm.vue')
    //     }
    //   ]
    // },
    // {
    //   path: '/',
    //   name: 'StatusView',
    //   component: () =>
    //       import('./views/drowningLayouts/StatusView.vue'),
    //   children:[
    //     {
    //       path: '/status',
    //       name: 'Status',
    //       props: true,
    //       component: () =>
    //           import('./components/drowning/Status.vue')
    //     },
    //   ]
    // },
  ]
})
