import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Multiselect from "vue-multiselect";
import * as VueGoogleMaps from 'vue2-google-maps'
import './main.scss'
import Swal from "sweetalert2";

Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(require('vue-moment'));
Vue.component('multiselect', Multiselect);
Vue.use(VueGoogleMaps, {
  load: {
    //set google map api key
    key: 'AIzaSyCIQdIJU4n2m_IssZOT6EGbUn5RJ9xcVKo',
    libraries: 'places',
    lang: 'th'
  },
  installComponents: true

})
Vue.prototype.$swal = Swal

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
