<?php  $menus = SiteHelpers::menus('top') ;?>
<ul class="nav navbar-nav  navbar-right">
    <li><a href="<?php echo e(url('')); ?>"> Home</a></li>
    <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($menu['module'] =='separator'): ?>
        <li class="divider"></li>        
        <?php else: ?>
            <li><!-- HOME -->
                <a 
                <?php if($menu['menu_type'] =='external'): ?>
                    href="<?php echo e(URL::to($menu['url'])); ?>" 
                <?php else: ?>
                    href="<?php echo e(URL::to($menu['module'])); ?>" 
                <?php endif; ?>
             
                 <?php if(count($menu['childs']) > 0 ): ?> class="dropdown-toggle" data-toggle="dropdown" <?php endif; ?>>
                    <i class="<?php echo e($menu['menu_icons']); ?>"></i>                 
                    <?php if(config('sximo.cnf_multilang') ==1 && isset($menu['menu_lang']['title'][session('lang')]) && $menu['menu_lang']['title'][session('lang')]!=''): ?>
                        <?php echo e($menu['menu_lang']['title'][session('lang')]); ?>

                    <?php else: ?>
                        <?php echo e($menu['menu_name']); ?>

                    <?php endif; ?>             
                    <?php if(count($menu['childs']) > 0 ): ?>
                     <b class="caret"></b> 
                    <?php endif; ?>  
                </a> 
                <?php if(count($menu['childs']) > 0): ?>
                <ul class="dropdown-menu dropdown-menu-right">
                <?php $__currentLoopData = $menu['childs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($menu2['module'] =='separator'): ?>
                        <li class="divider"> </li>        
                    <?php else: ?>
                    <li class="
                        <?php if(count($menu2['childs']) > 0): ?> dropdown-submenu <?php endif; ?>
                        <?php if(Request::is($menu2['module'])): ?> active <?php endif; ?>">
                        <a 
                            <?php if($menu2['menu_type'] =='external'): ?>
                                href="<?php echo e(url($menu2['url'])); ?>" 
                            <?php else: ?>
                                href="<?php echo e(url($menu2['module'])); ?>" 
                            <?php endif; ?>
                                        
                        >
                            <i class="<?php echo e($menu2['menu_icons']); ?>"></i> 
                            <?php if(config('sximo.cnf_multilang') ==1 && isset($menu2['menu_lang']['title'][session('lang')])): ?>
                                <?php echo e($menu2['menu_lang']['title'][session('lang')]); ?>

                            <?php else: ?>
                                <?php echo e($menu2['menu_name']); ?>

                            <?php endif; ?>                        
                        </a>
                    <?php endif; ?>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                </ul>
                <?php endif; ?>
            </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
    <li class="dropdown">
        <a href="javascript://ajax" class="dropdown-toggle" data-toggle="dropdown"> My Account <span class="caret"></span> </a>
         <ul class="dropdown-menu ">
          <?php if(Auth::check()): ?>
            <li><a href="<?php echo e(url('dashboard')); ?>">Dashboard </a></li>
            <li class="divider"></li>
           <li><a href="<?php echo e(url('user/profile?view=frontend')); ?>"> <?php echo e(__('core.m_profile')); ?></a></li>
           
           <li><a href="<?php echo e(url('user/logout')); ?>"> <?php echo e(__('core.m_logout')); ?> </a></li>
           <?php else: ?>
           
            <li><a href="<?php echo e(url('user/login')); ?>" onclick="SximoModal(this.href , '<?php echo e(__('core.signin')); ?>'); return false;"> <?php echo e(__('core.signin')); ?> </a></li>
            <li><a href="<?php echo e(url('user/register')); ?>" onclick="SximoModal(this.href , '<?php echo e(__('core.signup')); ?>'); return false;"> <?php echo e(__('core.signup')); ?> </a></li>
           <?php endif; ?>  
       </ul> 
    </li>    
</ul>            

