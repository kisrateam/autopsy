<?php $sidebar = SiteHelpers::menus('sidebar') ;?>

  <div class="navbar"> </div>
 <div class="menu">
       <ul class="list">
        <div class="user-info">
                <div class="image">
                    <?php echo SiteHelpers::avatar( 48 ); ?>

                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo e(session('fid')); ?></div>
                    <div class="email"><?php echo e(session('eid')); ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo e(url('user/profile')); ?>"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo e(url('user/logout')); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <li class="header">MAIN NAVIGATION</li>
            <li >
                        <a href="<?php echo e(url('dashboard')); ?>">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
      
        <?php $__currentLoopData = $sidebar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            

             <li <?php if(Request::segment(1) == $menu['module']): ?> class="active" <?php endif; ?>>

            <?php if($menu['module'] =='separator'): ?>
            <li class="header"> <span> <?php echo e($menu['menu_name']); ?> </span></li>

                
            <?php else: ?>


                <a data-toggle="tooltip" title="<?php echo e($menu['menu_name']); ?>" data-placement="right"

                    <?php if(count($menu['childs']) > 0 ): ?> 
                        href="javascript:void(0);" 

                    <?php elseif($menu['menu_type'] =='external'): ?>
                        href="<?php echo e($menu['url']); ?>" 
                    <?php else: ?>
                        href="<?php echo e(URL::to($menu['module'])); ?>" 
                    <?php endif; ?>              
                
                 <?php if(count($menu['childs']) > 0 ): ?> class="menu-toggle" <?php endif; ?>>
                    <i class="<?php echo e($menu['menu_icons']); ?>"></i> 
                    <span>                    
                        <?php echo e((isset($menu['menu_lang']['title'][session('lang')]) ? $menu['menu_lang']['title'][session('lang')] : $menu['menu_name'])); ?>

                    </span> 
                      
                </a> 
                <?php endif; ?>  

                <?php if(count($menu['childs']) > 0): ?>
                    <ul class="ml-menu">
                        <?php $__currentLoopData = $menu['childs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <li <?php if(Request::segment(1) == $menu2['module']): ?> class="active" <?php endif; ?>>
                            <a  
                                <?php if($menu2['menu_type'] =='external'): ?>
                                    href="<?php echo e($menu2['url']); ?>" 
                                <?php else: ?>
                                    href="<?php echo e(URL::to($menu2['module'])); ?>"  
                                <?php endif; ?>                                  
                            >
                            
                            
                            <span><?php echo e((isset($menu2['menu_lang']['title'][session('lang')]) ? $menu2['menu_lang']['title'][session('lang')] : $menu2['menu_name'])); ?>

                            </span></a> 
                            <?php if(count($menu2['childs']) > 0): ?>
                            <ul class="nav nav-third-level">
                                <?php $__currentLoopData = $menu2['childs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li <?php if(Request::segment(1) == $menu3['module']): ?> class="active" <?php endif; ?>>
                                        <a 
                                            <?php if($menu['menu_type'] =='external'): ?>
                                                href="<?php echo e($menu3['url']); ?>" 
                                            <?php else: ?>
                                                href="<?php echo e(URL::to($menu3['module'])); ?>" 
                                            <?php endif; ?>                                      
                                        
                                        >
                                       
                                        <span> 
                                        <?php echo e((isset($menu3['menu_lang']['title'][session('lang')]) ? $menu3['menu_lang']['title'][session('lang')] : $menu3['menu_name'])); ?>       </span>                                  
                                            
                                        </a>
                                    </li>   
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <?php endif; ?>                          
                        </li>                           
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </ul>   
</div>