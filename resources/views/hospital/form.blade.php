@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'hospital?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-default  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-default"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Hospital</legend>
									
									  <div class="form-group  " >
										<label for="Hospcode" class=" control-label col-md-4 text-left"> Hospcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='hospcode' id='hospcode' value='{{ $row['hospcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name" class=" control-label col-md-4 text-left"> Name <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='name' id='name' value='{{ $row['name'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Typecode" class=" control-label col-md-4 text-left"> Typecode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='typecode' id='typecode' value='{{ $row['typecode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type" class=" control-label col-md-4 text-left"> Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='type' id='type' value='{{ $row['type'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ministry" class=" control-label col-md-4 text-left"> Ministry <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ministry' id='ministry' value='{{ $row['ministry'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ministryname" class=" control-label col-md-4 text-left"> Ministryname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ministryname' id='ministryname' value='{{ $row['ministryname'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Departcode" class=" control-label col-md-4 text-left"> Departcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='departcode' id='departcode' value='{{ $row['departcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Depart" class=" control-label col-md-4 text-left"> Depart <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='depart' id='depart' value='{{ $row['depart'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Changwatcode" class=" control-label col-md-4 text-left"> Changwatcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='changwatcode' id='changwatcode' value='{{ $row['changwatcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ampurcode" class=" control-label col-md-4 text-left"> Ampurcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ampurcode' id='ampurcode' value='{{ $row['ampurcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tamboncode" class=" control-label col-md-4 text-left"> Tamboncode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='tamboncode' id='tamboncode' value='{{ $row['tamboncode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tel" class=" control-label col-md-4 text-left"> Tel <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='tel' id='tel' value='{{ $row['tel'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Zip" class=" control-label col-md-4 text-left"> Zip <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='zip' id='zip' value='{{ $row['zip'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type1" class=" control-label col-md-4 text-left"> Type1 <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='type1' id='type1' value='{{ $row['type1'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Region" class=" control-label col-md-4 text-left"> Region <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='region' id='region' value='{{ $row['region'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Active" class=" control-label col-md-4 text-left"> Active <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Active' id='Active' value='{{ $row['Active'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("hospital/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop