<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class hospital extends Sximo  {
	
	protected $table = 'hospcode';
	protected $primaryKey = 'hospcode';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT hospcode.* FROM hospcode  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE hospcode.hospcode IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
