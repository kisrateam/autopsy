<?php
        
// Start Routes for usertype 
Route::resource('usertype','UsertypeController');
// End Routes for usertype 

                    
// Start Routes for doctors 
Route::resource('doctors','DoctorsController');
// End Routes for doctors 

                    
// Start Routes for officers 
Route::resource('officers','OfficersController');
// End Routes for officers 

                    
// Start Routes for injurytype 
Route::resource('injurytype','InjurytypeController');
// End Routes for injurytype 

                    
// Start Routes for deathreasom 
Route::resource('deathreasom','DeathreasomController');
// End Routes for deathreasom 

                    
// Start Routes for cicumstances 
Route::resource('cicumstances','CicumstancesController');
// End Routes for cicumstances 

                    
// Start Routes for policestation 
Route::resource('policestation','PolicestationController');
// End Routes for policestation 

                    
// Start Routes for userautopsy 
Route::resource('userautopsy','UserautopsyController');
// End Routes for userautopsy 

                    
// Start Routes for hospital 
Route::resource('hospital','HospitalController');
// End Routes for hospital 

                    ?>